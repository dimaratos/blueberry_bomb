const path = require('path');
module.exports={
    mode: 'development',
    watch:true,
    entry: path.resolve(__dirname, "src", "Spiel.js"),
    devtool: "source-map",
    output: {
        filename : 'bundle.js',
        path : path.resolve(__dirname, "build"),

    },


};