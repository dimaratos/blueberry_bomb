

const CANVAS_WIDTH = 1800;
const CANVAS_HEIGHT = 920;
const SpriteSize = 60 ;
const MARGIN = 100;
const U1 = 0.90 * CANVAS_HEIGHT;
const O1 = 0.75 * CANVAS_HEIGHT;
const O2 = CANVAS_HEIGHT * 0.5; //obere Schranke von Level 2
const U2 = CANVAS_HEIGHT * 0.7;//Untere Schranke von Level 2
const U3 = CANVAS_HEIGHT * 0.3;
const minLifeBlock = 10;
const maxLifeBlock = 30;
const SECOND =1000;
const NUMBER_BEER = 20;
const width_Block = 50 ;
const width_Beer = 20 ;
const height_Beer = 40;




const express = require('express');
const app = express();
const http = require('http');
const server = require('http').Server(app);
const io = require('socket.io')(server, {});
const port = 8000;

app.get('/',function (req,res) {
    res.sendFile(__dirname + '/index.html');
});
app.use( express.static('.'));
server.listen(port, function () {

    console.log('Server started and is listening to  port %d', port);
});




let positionPlayers = [];
let positionBeers = [];
let positionLvl1 = [];
let positionLvl2 = [];
let positionLvl3 = [];



const generateLevels = () => {

    //generating level1
    for (let i = 0; i < CANVAS_WIDTH; i += MARGIN)
    {
            let randomID = '_' + Math.random().toString(36).substr(2, 9);
            let j = Math.floor(Math.random() * (U1 - O1)) + O1;
            let life = Math.floor(Math.random() * (maxLifeBlock-minLifeBlock+1) + minLifeBlock) * SECOND;
            let Taken = false ;
        positionLvl1.push({id: randomID, x: i, y: j,life:life,Taken:Taken});
    }

    //generating level1
    for (let i = 0; i < CANVAS_WIDTH; i += MARGIN)
    {
        let randomID = '_' + Math.random().toString(36).substr(2, 9);
        let j = Math.floor(Math.random() * (U2-O2+0.1) + O2);
        let life = Math.floor(Math.random() * (maxLifeBlock-minLifeBlock+1) + minLifeBlock) * SECOND;
        let Taken = false;
        positionLvl2.push({id: randomID, x: i, y: j,life:life,Taken:Taken});
    }

    let allBlocks = positionLvl1.concat(positionLvl2);

    //generating Beers

    while (positionBeers.length < NUMBER_BEER)
    {

            let father = Math.floor(Math.random() * allBlocks.length);

            if(!allBlocks[father].Taken){
                allBlocks[father].Taken = true ;
                let x = (allBlocks[father].x + width_Block/2) - width_Beer ;
                let y = allBlocks[father].y - height_Beer ;
                let randomID = '_' + Math.random().toString(36).substr(2, 9);
                positionBeers.push({id: randomID,x:x,y:y,father:father,isTaken:false});
            }
    }
};

generateLevels();

io.on('connection', function(socket) {

    let name = "";

    socket.on('login_player', function (data) {

        let playerDetails = {
            id: data.id,
            x: 0,
            y: CANVAS_HEIGHT - SpriteSize,
            hasBeer : false,
            thisBeerIsMine : -1
        };
        name = data.id;

        switch (positionPlayers.length) {
            case 0:
                console.log(playerDetails);
                playerDetails.isHost = true;
                console.log(playerDetails);
                break;
            case 1:
                playerDetails.x = (CANVAS_WIDTH - 2*SpriteSize);
                playerDetails.y = CANVAS_HEIGHT - SpriteSize;
                playerDetails.isHost = false;
                console.log(playerDetails);
                break;
            default:
                break;
        }
        // store incoming player
        positionPlayers.push(playerDetails);
        console.log(positionPlayers);
        // create incoming player
        io.sockets.emit('create_player', playerDetails);


        // create rest of all currently attending player

            for (let i = 0; i < positionPlayers.length - 1; i++) {
                socket.emit('create_player', positionPlayers[i]);
                console.log(positionPlayers[i]);
            }


        // send all blocks from level1 to incoming player
        socket.emit('Generate_Level_1', [...positionLvl1]);
        // send all blocks from level1 to incoming player


        // send all blocks from level2 to incoming player

        socket.emit('Generate_Level_2', [...positionLvl2]);

        // send all beers from array beers to incoming player
        socket.emit('Generate_Beer', [...positionBeers]);


        // send the update of  all blocks from level1 to incoming player

        socket.on('move_Level_1',function (data)
        {
            socket.broadcast.emit('move_Level_1', data);

        });
        // send the update of  all blocks from level2 to incoming player

        socket.on('move_Level_2', function(data)
        {
                    socket.broadcast.emit('move_Level_2', data);
        });





    });
    // disconnection of player
    socket.on('disconnect', function() {
        for (let i = 0; i < positionPlayers.length; i++) {
            if (name === positionPlayers[i].id) {
                positionPlayers.splice(i,1);
                socket.broadcast.emit('timeout', {id: name});
                console.log("player is offline  " + name);
            }
        }
    });

    //update of avatar
    socket.on('move_Avatar', function(data)
    {

        socket.broadcast.emit('move_Avatar', data);
        if (data.hasBeer) {
            data.score += 10;
            socket.emit('Scores', data);
        }
        positionPlayers.forEach(player => {
        if (player.id === data.id) {
            player.x = data.x;
            player.y = data.y;
            player.score = data.score;
        }
    });



    });

    //update of beer
    socket.on('move_Beer', function(data)
    {

        positionBeers.forEach(beer => {
            if (beer.id === data.id) {
                beer.x = data.x;
                beer.y = data.y;
            }

        });
        socket.broadcast.emit('move_Beer', data);
    });
});
