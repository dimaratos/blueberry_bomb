##How to start the server instance and play the game Beer Bomb


To play the game, please navigate in your terminal to the repository `blueberry_bomb` and enter the following command to start a server instance:

* `./node_modules/.bin/webpack`

In another terminal window, please navigate to the repository `blueberry_bomb` and enter below command:

* `./node server.js`

In your web browser, please go to http://localhost:8000/.

### Playing the Game

The goal of the game is to get the highest score, which is related to the collection of beers in order to be able to exit. To move your avatar, use the ArrowLeft key to move left, the ArrowRight key to move right and the ArrowUp key to jump on the blocks.

To win the game, the avatar must get the highest score and reach the beer barrel in the upper screen after collecting all the beers, he will increase his score by 10 point per beer. The game is over as soon as one of the two avatars hits the exit and there are no more beers on the field. The prize the winner gets is a barrel full of beer.

The small beer barrels sitting on the blocks can be taken by the avatars simply by jumping on the corresponding block and colliding with the small beer barrel. Another way an avatar can score points is ram into his competitor which adds 5 points to his score.
