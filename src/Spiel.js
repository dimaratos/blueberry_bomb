import Spielfeld from './Spielfeld';
import AssetLoader from "./AssetLoader";

//Assetloader and Connection
new AssetLoader()
    .loadAssets([

        {name: 'gameoverscreen', url: '../src/assets/gameoverscreen.png'},
        {name:'player1', url: '../src/assets/avatar1.png' },
        {name: 'player2', url: '../src/assets/avatar2.png' },
        {name: 'beer', url: '../src/assets/beer.png' },
        {name: 'beerbarrelExit', url: '../src/assets/beer_barrel.png' },

        {name: 'beerbarrelbottom', url: '../src/assets/beerbarrelbottom.jpg'},
        {name: 'block', url: '../src/assets/block.png'}])
    .then(assets => {
        // Assets are loaded at this point and saved in assets.

                // unique id
                let id =  '_' + Math.random().toString(36).substr(2, 9);

                // initialize game
                new Spielfeld('myCanvas', assets, id);

    });