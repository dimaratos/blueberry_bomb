const canvas = document.getElementById('myCanvas');
const CANVAS_WIDTH = canvas.width;
const CANVAS_HEIGHT = canvas.height;
const O2 = CANVAS_HEIGHT * 0.5; //obere Schranke von Level 2
const U2 = CANVAS_HEIGHT * 0.7;//Untere Schranke von Level 2
const U3 = CANVAS_HEIGHT * 0.3;
const MARGIN = 60;
const BOX =20;
const minLifeBlock = 10;
const maxLifeBlock = 30;
const SECOND =1000;
const distanceExit = 5;
const MARGIN_GEAR =30;
const FPS =1000/30;


export  {U3,FPS,canvas,O2,U2,CANVAS_WIDTH,CANVAS_HEIGHT,MARGIN,BOX,minLifeBlock,maxLifeBlock,SECOND, distanceExit,MARGIN_GEAR}

