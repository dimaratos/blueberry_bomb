export default class Level_2 {
    constructor(assets) {
        this.assets = assets;
        this.lvl2 = [];
    }

    draw(context) {
        for (let i = 0; i <= this.lvl2.length - 1; i++) {
            this.lvl2[i].draw(context);
        }
    }
}