import {CANVAS_HEIGHT, CANVAS_WIDTH} from "./Const";
import { BOX } from "./Const";


export default class Avatar {

    constructor(x, y, assets, spriteSize, game, id) {

        this.x = x;
        this.y = y;
        this.game = game;
        this.assets = assets;
        this.spriteSize = spriteSize;
        this.id = id;
        this.width = BOX * 5;
        this.height = BOX;

        this.speed_unit_x = 15;
        this.speed_unit_y = -20;

        this.max_jump_height = 150;
        this.gravity_unit = 20;

        this.speed_x = 0;
        this.speed_y = 0;
        this.air_up = false;
        this.air_down = false;

        this.score = 0;
        this.hasBeer = false;
        this.throwRight = false;
        this.throwLeft = false;
        this.thisBeerIsMine =-1;
    }


    getCords() {
        return {
            x: this.x,
            y: this.y
        }
    }


    draw(ctx) {
        this.player = this.assets.player1;
        ctx.drawImage(
            this.player,
            this.x,
            this.y,
            this.spriteSize,
            this.spriteSize
        );
    }

    react_key_down(event) {

        let key = event.key;

        if (key === 'ArrowLeft') {
            this.speed_x = -this.speed_unit_x;
        }
        else if (key === 'ArrowRight') {
            this.speed_x = this.speed_unit_x;
        }
        else if (key === 'ArrowUp') {
            if (this.air_up === false && this.air_down === false) {
                this.speed_y = this.speed_unit_y;
                this.air_up = true;
                this.air_down = false;
                this.jump_height = this.y - this.max_jump_height;
            }
        }

        else if (key === 'x' && this.hasBeer === true) {
            console.log('x pressed');
            this.throwRight = true;
        }

        else if (key === 'z' && this.hasBeer === true) {
            //console.log('z pressed');
            this.throwLeft = true;
        }
    }

    react_key_up(event) {
        let key = event.key;
        if (key === 'ArrowLeft') {
            this.speed_x = 0;
        } else if (key === 'ArrowRight') {
            this.speed_x = 0;
        } else if (key === 'ArrowDown') {
            console.log('Arrow Down released');
        }
    }

    update(blocks) {

        // update player position depending on block
        if (this.on_block === true) {
            this.x = this.which_block.x + this.block_dx;
            this.y = this.which_block.y + this.block_dy;
        }

        // move on x axis
        this.x += this.speed_x;
        
        // handles the jumping
        if (this.air_up) {
            this.y += this.speed_y;
        }
        if (this.air_down) {
            this.y += this.gravity_unit;
        }
        
        // check blocks and exit
        var px1 = this.x;
        var py1 = this.y;
        var px2 = this.x + this.spriteSize;
        var py2 = this.y + this.spriteSize;


        var hit = false;
        for (var i = 0; i < blocks.length; i++) {
            var bx1 = blocks[i].x;
            var by1 = blocks[i].y;
            var bx2 = blocks[i].x + blocks[i].width;
            var by2 = blocks[i].y + blocks[i].height;

            // test if player hits the block
            if ((px1 <= bx2) && (px2 >= bx1) && (py2 <= by2) && (py2 >= by1)) {
                this.air_up = false;
                this.air_down = false;
                this.y = by1 - this.spriteSize;
                hit = true;

                // store state if on block
                this.on_block = true;
                this.which_block = blocks[i];
                this.block_dx = this.x - this.which_block.x;
                this.block_dy = this.y - this.which_block.y;
            }
        }
        // if not hit yet, then not on block
        if (hit === false) {
            this.on_block = false;
        }
        // when down on the ground --- kein haken der taste mehr
        if ((hit === false) && (this.y >= CANVAS_HEIGHT - this.spriteSize)) {
            this.air_up = false;
            this.air_down = false;
            this.y = CANVAS_HEIGHT - this.spriteSize;
            hit = true;
        }

        if ((hit === false) && (this.air_up === false)) {
            this.air_up = false;
            this.air_down = true;
        }

        if (hit === false && this.air_up === true && this.y <= this.jump_height) {
            this.air_up = false;
            this.air_down = true;
        }

        if (hit === true) {
            this.air_up = false;
            this.air_down = false;
        }


        //                  Server Seite
        //###################################################//


        //this.game.broadcastPosition({ id: this.id, x: this.x, y: this.y })
    }


    touch(object) {
        return !(object.x > (this.x + this.width) ||
            (object.x + object.width) < this.x ||
            object.y > (this.y + this.height) ||
            (object.y + object.height) < this.y);
    }
}