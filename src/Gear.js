import {BOX, MARGIN_GEAR} from "./Const";
import Block from "./Block";

export default class Gear {
    constructor(x, y, rotation, radius, assets) {
        this.assets = assets;
        this.rotation = rotation;
        this.x = x;
        this.y = y;
        this.lvl3 = [];
        this.angle = 0;
        this.velocity = 0.01;
        this.radius = radius;
        this.fill();
    }

    //fill each gear with 8 surrounded blocks
    fill() {
        this.b1 = new Block(this.x - BOX, this.y - 70 - MARGIN_GEAR - BOX, this.assets);
        this.b2 = new Block(this.x + 70 + MARGIN_GEAR, this.y - BOX * 0.5, this.assets);
        this.b3 = new Block(this.x - BOX, this.y + 70 + MARGIN_GEAR, this.assets);
        this.b4 = new Block(this.x - 70 - MARGIN_GEAR - 2 * BOX, this.y - BOX * 0.5, this.assets);
        this.b5 = new Block((this.b1.x + this.b2.x) / 2, (this.b1.y + this.b2.y) / 2, this.assets);
        this.b6 = new Block((this.b3.x + this.b2.x) / 2, (this.b3.y + this.b2.y) / 2, this.assets);
        this.b7 = new Block((this.b3.x + this.b4.x) / 2, (this.b3.y + this.b4.y) / 2, this.assets);
        this.b8 = new Block((this.b1.x + this.b4.x) / 2, (this.b1.y + this.b4.y) / 2, this.assets);
        this.lvl3.push(this.b1);
        this.lvl3.push(this.b2);
        this.lvl3.push(this.b3);
        this.lvl3.push(this.b4);
        this.lvl3.push(this.b5);
        this.lvl3.push(this.b6);
        this.lvl3.push(this.b7);
        this.lvl3.push(this.b8);
    }

    get_blocks() {
        return this.lvl3;
    }

    draw(ctx) {
        //draw the central gear
        ctx.beginPath();
        ctx.fillStyle = "#FF4422";
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
        //draw the blocks
        for (let i = 0; i < this.lvl3.length; i++) {
            this.lvl3[i].draw(ctx);
        }
    }

    move() {
        for (let i = 0; i < this.lvl3.length; i++) {
            if (this.rotation === "indirect") {
                this.angle += (this.velocity - (Math.PI / 4));
            } else if (this.rotation === "direct") {
                this.angle -= (this.velocity - (Math.PI / 4));
            }
            this.lvl3[i].x = this.x + this.radius * Math.cos(this.angle);
            this.lvl3[i].y = this.y + this.radius * Math.sin(this.angle);
        }
    }
}
