import {BOX, CANVAS_WIDTH, MARGIN, maxLifeBlock, minLifeBlock, O2, SECOND, U2} from "./Const";

export default class Block {

    constructor(x, y,assets,id,life,game) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.height = BOX;
        this.width = 2.5 * BOX;
        //every Block has a random lifetime between 10 and 30 secondes
        this.life = life;
        this.Blinkflag = false;
        this.Taken = false;
        this.assets = assets;
        this.game = game ;
    }

    draw(context) {
        this.block = this.assets.block;
        let color;
        if (this.Blinkflag) {
            color = "red";
            context.fillStyle = color;
            context.fillRect(
                this.x,
                this.y,
                this.width,
                this.height
            );
        } else {
            context.drawImage(
                this.block,
                this.x,
                this.y,
                this.width,
                this.height
            );
        }
    }

    setTimer(){

        //a Timer is set in every Block of the Array
        setInterval(function () {
            this.y =Math.floor(Math.random() * (U2-O2+0.1)+ O2);
            this.game.blockLevel2({id:this.id, x :this.x, y:this.y});
            this.Blinkflag = false;
        }.bind(this), this.life);


    }

    setTimerAlert() {
        setTimeout(function () {
            this.Blinkflag = true;
            setInterval(function () {
                this.Blinkflag = true;
                this.game.blockLevel2({id:this.id, x :this.x, y:this.y,life :this.life});
            }.bind(this), this.life);
        }.bind(this), this.life * 0.8);

    }

    move() {
        this.x += 10;
        this.game.blockLevel1({id:this.id, x :this.x, y:this.y});

    }



    getCords() {
        return {
            x: this.x,
            y: this.y,
            life: this.life
        }
    }

}
