import Block from "./Block";
import { CANVAS_HEIGHT, CANVAS_WIDTH } from "./Const";



export default class Level_1 {
    constructor(assets) {
        this.assets = assets;
        this.lvl1 = [];

    }

    get_blocks() {

        return this.lvl1;
    }


    draw(context) {
        this.lvl1.forEach(function (value, index, array) {
            array[index].draw(context);
        });
    }


    update() {
        for (let i = 0; i <= this.lvl1.length - 1; i++) {
            if (this.lvl1[i].x + (this.lvl1[i].width / 2) > CANVAS_WIDTH) {
                this.lvl1[i].x = -10;
            }
            this.lvl1[i].move();
        }

    }


}


