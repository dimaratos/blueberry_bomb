import {BOX, CANVAS_WIDTH, MARGIN, maxLifeBlock, minLifeBlock, O3, SECOND, U3} from "./Const";

export default class Exit {

    constructor(x, y, assets) {
        //console.log('Exit created');
        this.x = x;
        this.y = y;
        this.assets = assets;
        this.width = BOX*12;
        this.height = BOX*6;

    }

    getCords(){
        return {
            x: this.x,
            y: this.y
        }
    }

    draw(ctx) {
        ctx.drawImage(
            this.assets.beerbarrelExit,
            this.x,
            this.y,
            this.width,
            this.height
        );
    }
}
/*
    isCollidingWith(block) {
        if ((this.avatar.x == this.block.x) && (this.avatar.y == this.block.y)) {
            return true;
        }
        return false;
    }*/