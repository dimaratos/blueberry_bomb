import {CANVAS_WIDTH, U3} from "./Const";
import gear from "./Gear";

export default class Level_3 {
    constructor(assets) {
        this.gear1 = new gear(CANVAS_WIDTH * 0.15, U3, 'direct', 130, assets);
        this.gear2 = new gear(CANVAS_WIDTH * 0.4, U3, 'indirect', 90, assets);
        this.gear3 = new gear(CANVAS_WIDTH * 0.6, U3, 'direct', 90, assets);
        this.gear4 = new gear(CANVAS_WIDTH * 0.85, U3, 'indirect', 130, assets);
        this.lvl3 = this.gear1.lvl3.concat(this.gear2.lvl3, this.gear3.lvl3, this.gear4.lvl3);
    }

    draw(ctx) {
        this.gear1.draw(ctx);
        this.gear2.draw(ctx);
        this.gear3.draw(ctx);
        this.gear4.draw(ctx);
    }

    update() {
        this.gear1.move();
        this.gear2.move();
        this.gear3.move();
        this.gear4.move();
    }
}