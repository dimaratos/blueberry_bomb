import { BOX } from "./Const";

export default class Beer {
    constructor(x,y,father,assets,id) {
        this.width = BOX;
        this.height = BOX * 2;
        this.father = father;
        this.x = x;
        this.y = y;
        this.isTaken = false;
        this.id = id ;
        this.assets = assets;

    }

    // draw the beer
    draw(ctx) {
        this.beer = this.assets.beer;
        ctx.drawImage(
            this.beer,
            this.x,
            this.y,
            this.width,
            this.height
        );

    }

    // get cords of the beers
    getCords() {
        return {
            id:this.id,
            x: this.x,
            y: this.y
        }
    }
}