import Avatar from './Avatar';
import Exit from './Exit';
import Level_1 from "./Level_1";
import Level_2 from "./Level_2";
import {BOX, CANVAS_HEIGHT, CANVAS_WIDTH, distanceExit, FPS} from "./Const";
import Level_3 from "./Level_3";
import Beer from "./Beer";
import Block from "./Block";
import io from 'socket.io-client'

export default class Spielfeld {
    constructor(canvasID, assets, id) {
        this.id = id;
        this.canvas = document.getElementById(canvasID);
        this.ctx = this.canvas.getContext('2d');
        this.assets = assets;
        this.players = [];
        this.level1 = new Level_1(this.assets);
        this.level2 = new Level_2(this.assets);
        //background image
        this.background = new Image();
        this.background.src = '../src/assets/sand_template.jpg';
        this.gameOver = false;
        this.isHost = false;
        this.text_x = 850;
        this.text_orig_y = CANVAS_HEIGHT / 3;
        this.text_y = this.text_orig_y;
        this.t = 0;
        this.smiley_orig_x = 850;
        this.smiley_orig_y = 550;
        this.smiley_x = this.smiley_orig_x;
        this.smiley_y = this.smiley_orig_y;
        this.score = 0;
        this.enemyScore = 0;
        this.newBeer = undefined;
        this.my_player_id = "";
        //                  Server Seite
        //###################################################//
        this.socket = io.connect('');
        // send notification to server in order to create your player
        this.socket.emit('login_player', {id: this.id});
        //create player function
        this.socket.on('create_player', (data) => {
            this.pushPlayer(data);
        });
        //disconnection
        this.socket.on('timeout', (data) => {
            this.disconnected(data);
        });
        // after logging in your player, the server will send you all generated blocks of level1
        this.socket.on('Generate_Level_1', (data) => {
            data.forEach(d => {
                this.pushBlock_Level_1(d);
            });
        });
        // after logging in your player, the server will send you all generated beers
        this.socket.on('Generate_Beer', (data) => {
            data.forEach(d => {
                this.pushBlock_Beers(d);
            });
            this.newBeer = this.beers;
        });
        // after logging in your player, the server will send you all generated blocks of level2
        this.socket.on('Generate_Level_2', (data) => {
            data.forEach(d => {
                this.pushBlock_Level_2(d);
            });
            if (this.isHost) {
                this.level2.lvl2.forEach(d => {
                    d.setTimer();
                });
            }
        });
        // server receive score from opponent
        this.socket.on("score", (score, i) => {
            this.enemyScore = score;
            this.newBeer.splice(i, 1);
            this.checkScore();
        });
        // Avatar Movement
        this.socket.on('move_Avatar', (data) => {
            this.PlayerMoved(data);
        });
        //level 1 update
        this.socket.on('move_Level_1', (data) => {
            this.level1Moved(data);
        });
        //level 2 update
        this.socket.on('move_Level_2', (data) => {
            this.level2Moved(data);
        });
        //Beer update position
        this.socket.on('move_Beer', (data) => {
            this.BeerMoved(data);
        });
        //                   ENDE SERVER
        //###################################################//
        this.spriteSize = 60;
        this.exit = new Exit(
            this.canvas.width / 2 - BOX * 6,
            distanceExit,
            this.assets
        );
        this.level3 = new Level_3(this.assets);
        this.beers = [];
        this.startAnimating();
    }

    startAnimating() {
        this.frameTime = FPS;
        this.then = window.performance.now();
        this.animate(this.then);
    }

    animate(currentTime) {
        if (this.gameOver === false) {
            const now = currentTime;
            const elapsed = now - this.then;
            if (elapsed > this.frameTime) {
                this.then = now;
                this.clear_screen();
                this.render();
                this.draw();
            }
        } else {
            this.gameoverscreen();
            // move text
            this.text_x -= 5;
            if (this.text_x < -1500) {
                this.text_x = CANVAS_WIDTH;
            }
            this.text_y = this.text_orig_y + 25 * Math.sin(this.t);
            // move smiley
            this.smiley_y = this.smiley_orig_y + 50 * Math.cos(this.t);
            // increment argument for sin
            this.t += 0.1;
        }
        window.requestAnimationFrame(this.animate.bind(this));
    }

    // clear the whole canvas
    clear_screen() {
        this.ctx.clearRect(
            0,
            0,
            this.canvas.width,
            this.canvas.height
        );
    }

    // won message for the winner
    gameovermsgWon() {
        this.ctx.beginPath();
        this.ctx.font = "50px Verdana";
        // Create gradient
        var gradient = this.ctx.createLinearGradient(0, 0, this.canvas.width, 0);
        gradient.addColorStop("0", "magenta");
        gradient.addColorStop("0.5", "blue");
        gradient.addColorStop("1.0", "red");
        // Fill with gradient
        this.ctx.fillStyle = "MistBlue";
        this.ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.ctx.fillStyle = gradient;
        this.ctx.fillText("You have won a barrel full of Munich's finest beer! Your score is " + this.score + " and your enemy's score is " + this.enemyScore, this.text_x, this.text_y);
    }

    // lost message for the loser
    gameovermsglĹost() {
        this.ctx.beginPath();
        this.ctx.font = "50px Verdana";
        // Create gradient
        var gradient = this.ctx.createLinearGradient(0, 0, this.canvas.width, 0);
        gradient.addColorStop("0", "magenta");
        gradient.addColorStop("0.5", "blue");
        gradient.addColorStop("1.0", "red");
        // Fill with gradient
        this.ctx.fillStyle = "MistBlue";
        this.ctx.fillRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.ctx.fillStyle = gradient;
        this.ctx.fillText("You lost a barrel full of Munich's finest beer! Your score is " + this.score + " and your enemy's score is " + this.enemyScore, this.text_x, this.text_y);
    }

    // emoji after the message
    gameoversmiley() {
        // draw the first circle
        this.ctx.beginPath();
        this.ctx.arc(this.smiley_x, this.smiley_y, 25, 0, Math.PI * 2, true);
        // create Linear gradient fill style
        var grd = this.ctx.createLinearGradient(this.smiley_x, this.smiley_y, 50, 75);
        grd.addColorStop("0", "magenta");
        grd.addColorStop("0.025", "blue");
        grd.addColorStop("0.03", "red");
        // Set the fill style
        this.ctx.fillStyle = grd;
        this.ctx.fill();
        // Create left eye: x, y, radius, startAngle, endAngle
        this.ctx.beginPath();
        this.ctx.arc(this.smiley_x - 6, this.smiley_y - 5, 4, 0, Math.PI * 2, true);
        this.ctx.fillStyle = "#ffffff";
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.arc(this.smiley_x - 6, this.smiley_y - 5, 2, 0, Math.PI * 2, true);
        this.ctx.fillStyle = "#000000";
        this.ctx.fill();
        //Create right eye: x, y, radius, startAngle, endAngle
        this.ctx.beginPath();
        this.ctx.arc(this.smiley_x + 8, this.smiley_y - 5, 4, 0, Math.PI * 2, true);
        this.ctx.fillStyle = "#ffffff";
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.arc(this.smiley_x + 8, this.smiley_y - 5, 2, 0, Math.PI * 2, true);
        this.ctx.fillStyle = "#000000";
        this.ctx.fill();
        this.ctx.beginPath();
        this.ctx.arc(this.smiley_x, this.smiley_y + 5, 10, 0, Math.PI, false);
        this.ctx.stroke();
    }

    gameoverscreen() {
        this.clear_screen();
        if (this.checkScore()) {
            this.gameovermsgWon();
        } else {
            //console.log('You lost');
            this.gameovermsglĹost();
        }
        this.gameoversmiley();
    }

    // the start function for the whole game that will be called every frame
    render() {
        this.level1.update();
        this.level3.update();
        this.brewery();
        this.players.forEach(player => {
            if (player.id === this.my_player_id) {
                player.update(this.allBlocks());
                this.score = player.score;
            }
        });
        //check for Winning condition
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].touch(this.exit) && this.newBeer.length === 0) {
                console.log("Player with ID : " + this.players[i].id + " has won !");
                this.players[i].x = this.exit.x + this.exit.width / 2;
                this.players[i].y = this.exit.y + this.exit.height / 2;
                this.gameOver = true;
                this.winner = this.players[i].id;
            }
        }
        // send to server Beer cords after update
        this.beers.forEach(beer => {
            this.socket.emit('move_Beer', beer.getCords());
        });
        //check for collision -- this should one day be move into an update function
        for (let i = 0; i < this.players.length; i++) {
            for (let j = i + 1; j < this.players.length; j++) {
                let pi = this.players[i];
                let pj = this.players[j];
                let distance = Math.sqrt(Math.pow(pi.x - pj.x, 2) + Math.pow(pi.y - pj.y, 2));
                if (distance < this.spriteSize * 1.2) {
                    console.log('collision' + i + ' and ' + j);
                    // option 2 with blocking only
                    this.players[i].x -= this.players[i].speed_x * 5;
                    this.players[j].x -= this.players[j].speed_x * 5;
                    this.players[i].score += 5;
                }
            }
        }
        //update cords of avatar and send it to server
        this.players.forEach(player => {
            let data = {id: player.id, score: player.score, x: player.x, y: player.y, hasBeer: player.hasBeer};
            this.socket.emit('move_Avatar', data);
        });
    }

    // draw the canvas with all element inside for the game
    draw() {
        this.clear_screen();
        this.ctx.drawImage(this.background, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        this.level3.draw(this.ctx);
        this.exit.draw(this.ctx);
        this.ctx.font = "35px Comic Sans MS";
        this.ctx.fillStyle = "black";
        this.ctx.fillText("Your Score:  " + this.score, 20, 50);
        this.beers.forEach(beer => {
            beer.draw(this.ctx);
        });
        // draw all the players that entered the game
        this.players.forEach(player => {
            player.draw(this.ctx);
        });
        // draw the first level
        this.level1.lvl1.forEach(block => {
            block.draw(this.ctx);
        });
        // draw the second level
        this.level2.lvl2.forEach(block => {
            block.draw(this.ctx);
        });
    }

    brewery() {
        let allBlocks = this.allBlocks();
        //update beers
        for (let j = 0; j < this.players.length; j++) {
            for (let i = 0; i < this.beers.length; i++) {
                // check if its taken by the block to update her position
                if (!this.beers[i].isTaken) {
                    this.beers[i].x = (allBlocks[this.beers[i].father].x + allBlocks[this.beers[i].father].width / 2) - this.beers[i].width / 2;
                    this.beers[i].y = allBlocks[this.beers[i].father].y - this.beers[i].height;
                }
                // check if you touched a beer
                if (this.players[j].touch(this.beers[i])) {
                    this.beers[i].isTaken = true;
                    this.players[j].hasBeer = true;
                    this.players[j].score += 10;
                    this.newBeer.splice(i, 1);
                    this.socket.emit("Scores", this.score, i);
                    this.players[j].thisBeerIsMine = i;
                    break;
                }
            }
        }
    }

    // concat all levels in one array
    allBlocks() {
        return this.level1.lvl1.concat(this.level2.lvl2, this.level3.lvl3);
    }

    // push the data from server to array of Players
    pushPlayer(data) {
        let x = data.x;
        let y = data.y;
        // we expect, that there is no player with this particular ID (data.id)
        let doesContain = false;
        // checks if there's already a player with this ID
        this.players.forEach(player => {
            if (player.id === data.id) {
                doesContain = true;
            }
        });
        // If there is no player with this particular ID, create new Player
        if (!doesContain) {
            let newPlayer = new Avatar(x, y, this.assets, this.spriteSize, this, data.id);
            if (newPlayer.id === this.id && !this.gameOver) {
                document.addEventListener('keydown', newPlayer.react_key_down.bind(newPlayer));
                document.addEventListener('keyup', newPlayer.react_key_up.bind(newPlayer));
                this.isHost = data.isHost;
                this.my_player_id = newPlayer.id;
            }
            this.players.push(newPlayer);
        }
    }

    // push the data from server to array of level1
    pushBlock_Level_1(data) {
        let x = data.x;
        let y = data.y;
        let life = data.life;
        // we expect, that there is no block with this particular ID (data.id)
        let doesContain = false;
        // checks if there's already a block with this ID
        this.level1.lvl1.forEach(block => {
            if (block.id === data.id) {
                doesContain = true;
            }
        });
        // If there is no block with this particular ID, create new block
        if (!doesContain) {
            let newBlock = new Block(x, y, this.assets, data.id, life, this);
            this.level1.lvl1.push(newBlock);
        }
    }

    // push the data from server to array of level2
    pushBlock_Level_2(data) {
        let x = data.x;
        let y = data.y;
        let life = data.life;
        // we expect, that there is no block with this particular ID (data.id)
        let doesContain = false;
        // checks if there's already a block with this ID
        this.level2.lvl2.forEach(block => {
            if (block.id === data.id) {
                doesContain = true;
            }
        });
        // If there is no block with this particular ID, create new block
        if (!doesContain) {
            let newBlock = new Block(x, y, this.assets, data.id, life, this);
            this.level2.lvl2.push(newBlock);
        }
    }

    // push the data from server to array of beers
    pushBlock_Beers(data) {
        let father = data.father;
        let x = data.x;
        let y = data.y;
        // we expect, that there is no block with this particular ID (data.id)
        let doesContain = false;
        // checks if there's already a block with this ID
        this.beers.forEach(beer => {
            if (beer.id === data.id) {
                doesContain = true;
            }
        });
        // If there is no block with this particular ID, create new block
        if (!doesContain) {
            let beer = new Beer(x, y, father, this.assets, data.id);
            this.beers.push(beer);
        }
    }

    // disconnected player
    disconnected(data) {
        console.log("-------------------------------------------------------");
        this.players.forEach((player, i) => {
            if (this.players[i].id === data.id) {
                this.players.splice(i, 1);
            }
        });
    }

    // get the new cords of avatar from server after update
    PlayerMoved(data) {
        this.players.forEach(player => {
            if (player.id === data.id && player.id !== this.my_player_id) {
                player.x = data.x;
                player.y = data.y;
                player.score = data.score;
                this.enemyScore = data.score;
                //player.score = data.score;
                console.log("this " + this.score);
                console.log("data " + data.score);
            }
        });
    }

    // get the new cords of block of level1 from server after update
    level1Moved(data) {
        this.level1.lvl1.forEach(block => {
            if (block.id === data.id) {
                block.x = data.x;
                block.y = data.y;
            }
        });
    }

    // get the new cords of block of level2 from server after update
    level2Moved(data) {
        this.level2.lvl2.forEach(block => {
            if (block.id === data.id) {
                block.x = data.x;
                block.y = data.y;
                block.life = data.life;
            }
        });
    }

    // get the new cords of Beers from server after update
    BeerMoved(data) {
        this.beers.forEach(beer => {
            if (beer.id === data.id) {
                beer.x = data.x;
                beer.y = data.y;
            }
        });
    }

    //only the host send the update of all blocks from level1 to server
    blockLevel1(location) {
        if (this.isHost == true) {
            this.socket.emit('move_Level_1', location);
        }
    }

    //only the host send the update of all blocks from level2 to server
    blockLevel2(location) {
        if (this.isHost == true) {
            this.socket.emit('move_Level_2', location);
        }
    }

    // check the score of the avatar
    checkScore() {
        if (this.enemyScore > this.score) {
            return false;
        } else if (this.enemyScore === 0 && this.score === 0) {
            return true;
        } else {
            return true;
        }
    }
}